script_name("�������_�������")
script_author("THERION")
-- const
local GALAXY_IP = {"176.32.39.200", "176.32.39.199", "176.32.39.198"}
local CMD = "giveaway"
local MAX_DIST_SQRD = 5 * 5 -- => max distance = 5
local KEYS = {
   DRUGS = 0x31, -- 1 key
   M4 = 0x32, -- 2 key
   DEAGLE = 0x33 -- 3 key
}
-- const

local ev = require("samp.events")
local inicfg = require("inicfg")

local ini = inicfg.load({general = { on = true, drugs = 12 }}, thisScript().name .. ".ini")

-- prints message to SAMP Chat
local function log(msg)
   local format = "{FB4343}[%s]{FFFFFF}: %s{FFFFFF}."
   sampAddChatMessage(string.format(format, thisScript().name, msg), -1)
end

local function dist_sqrd(vec1, vec2)
   return (vec1[1] - vec2[1]) * (vec1[1] - vec2[1])
      + (vec1[2] - vec2[2]) * (vec1[2] - vec2[2])
      + (vec1[3] - vec2[3]) * (vec1[3] - vec2[3])
end

function main()
   if not doesFileExist(thisScript().name .. ".ini") then 
      inicfg.save(ini, thisScript().name .. ".ini") 
   end

   repeat wait(0) until isSampAvailable()

   do -- check if Galaxy-RPG
      local function is_in_array(array, value)
         for _, element in ipairs(array) do
            if value == element then
               return true
            end
         end
         return false
      end
      
      local ip, _ = sampGetCurrentServerAddress()
      if not is_in_array(GALAXY_IP, ip) then
         thisScript():unload()
      end   
   end

   sampRegisterChatCommand(CMD, function(args)
      local n_args = tonumber(args)
      if n_args then
         ini.general.drugs = n_args
         log(string.format("������ �� %d �������������", n_args))
      else
         ini.general.on = not ini.general.on
         log(ini.general.on and "�� ������" or "�������")
      end
   end)

   local execute = {
      DRUGS = function(id) 
         return string.format("/give drugs %d %d", id, ini.general.drugs)
      end,
      M4 = function(id)
         return string.format("/sellgun %d m4", id)
      end,
      DEAGLE = function(id)
         return string.format("/sellgun %d eagle", id)
      end
   }

   while true do wait(0)
      local is_busy = sampIsChatInputActive() or isSampfuncsConsoleActive() or isCharInAnyCar(PLAYER_PED)
      if not is_busy then
         local res, target = getCharPlayerIsTargeting(PLAYER_HANDLE)
         if res then
            local alive, id = sampGetPlayerIdByCharHandle(target)
            local tg_pos = {getCharCoordinates(target)}
            local local_pos = {getCharCoordinates(PLAYER_PED)}
            if alive and dist_sqrd(tg_pos, local_pos) < MAX_DIST_SQRD then
               for i, keycode in pairs(KEYS) do
                  if isKeyJustPressed(keycode) then
                     sampSendChat(execute[i](id))
                  end
               end
            end
         end
      end
   end
end