local GALAXY_IP = {"176.32.39.200", "176.32.39.199", "176.32.39.198"}
local CMD = "/piss"
local key = 0x58        -- VK_X
local key_clear = 0x46  -- VK_F
local wait_ping = 60

function main()
   repeat wait(0) until isSampAvailable()
   
   do -- check if Galaxy-RPG
      local function is_in_array(array, value)
         for _, element in ipairs(array) do
            if value == element then
               return true
            end
         end
         return false
      end
      
      local ip, _ = sampGetCurrentServerAddress()
      if not is_in_array(GALAXY_IP, ip) then
         thisScript():unload()
      end   
   end


   while true do wait(0)
      if isKeyJustPressed(key) then -- that's bad way of catching key press but ok for now
         local is_busy = sampIsChatInputActive() or isSampfuncsConsoleActive() or sampIsDialogActive() or isCharInAnyCar(PLAYER_PED)
         if not is_busy then
            sampSendChat(CMD)
            wait(wait_ping)
            setVirtualKeyDown(key_clear, true)
            wait(0)
            setVirtualKeyDown(key_clear, false)
         end
      end
   end
end