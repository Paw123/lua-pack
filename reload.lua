script_name("Reload")
script_author("THERION")
script_description("CTRL + R to reload scripts.")
script_properties("work-in-pause")

-- config
local delay = 40 -- delay before reloading scripts
local exceptions = { 
   -- contains file names of every exceptional script
   -- (not to be reloaded)
   "MapLimit-260.lua"
}
local keys = { -- keys binding
   0x11, -- VK_CONTROL
   0x52, -- VK_R
}
-- config

table.insert(exceptions, thisScript().filename)
local lua_ext = {"lua", "luac"}
local lfs   = require("lfs")

-- checks if value is in array
local function is_in_array(array, value)
   for _, element in ipairs(array) do
      if value == element then
         return true
      end
   end
   return false
end

local function reload_all()
   wait(delay)

   local reloaded = {}
   -- reloading all the currently active scripts
   for _, lua_script in pairs(script.list()) do
      if not is_in_array(exceptions, lua_script.filename) then
         lua_script:reload()
         table.insert(reloaded, lua_script.filename)
      end
   end

   -- load all terminated scripts from working directory
   for file in lfs.dir(getWorkingDirectory()) do
      local _, extension = file:match("(.+)%.(.+)")

      local is_lua = is_in_array(lua_ext, extension)
      local is_loaded = is_in_array(exceptions, file) or is_in_array(reloaded, file)

      if is_lua and not is_loaded then
         script.load(file)
      end
   end
end

function main()
   while true do wait(0)
      if isKeyDown(keys[1]) and isKeyJustPressed(keys[2]) then
         reload_all()
      end
   end
end
