script_name("Dealer420")
script_author("THERION")
script_url("https://gitlab.com/THXRION666")
-- config
local CMD = "deal420"
local DELAY = 1 
local AMOUNT = 12
local MAX_DISTANCE_SQUARED = 100 -- => 10m
-- config
local CMD_PATTERN = "/give drugs %d %d"
-- server data, can be updated

local mad = require("MoonAdditions")

local thread = lua_thread.create_suspended(function(queue)
   for _, id in ipairs(queue) do
      local clock = os.clock()
      
      local cmd = string.format(CMD_PATTERN, id, AMOUNT)
      sampSendChat(cmd)

      repeat wait(0) until os.clock() >= DELAY + clock
   end
end)

local function get_dist_sqrd(veh)
   local px, py, pz = getCharCoordinates(PLAYER_PED)
   local cx, cy, cz = getCarCoordinates(veh)
   return (px - cx) * (px - cx) + (py - cy) * (py - cy) + (pz - cz) * (pz - cz)
end

function main()
   repeat wait(0) until isSampAvailable()

   sampRegisterChatCommand(CMD,
   function()
      if thread:status() == "running" then 
         return 
      end
      
      local queue = {}
      local veh = isCharInAnyCar(PLAYER_PED) and storeCarCharIsInNoSave(PLAYER_PED) or select(1, storeClosestEntities(PLAYER_PED))
      
      if veh == -1 or get_dist_sqrd(veh) < MAX_DISTANCE_SQUARED then 
         return 
      end
      
      for _, ped in ipairs(getAllChars()) do
         if ped ~= PLAYER_PED then
            local x, y, z = getCharCoordinates(ped)
            local col, data = processLineOfSight(
               x, y, z, x, y, z - 2,
               false, true, false, false, 
               false, false, false, false)
            if col then
               local etype, _ = mad.get_entity_type_and_class(data.entity)
               local playing, id = sampGetPlayerIdByCharHandle(ped)

               if playing and etype == mad.entity_type.VEHICLE
               and veh == getVehiclePointerHandle(data.entity) then
                  table.insert(queue, id)
               end
            end
         end
      end
      thread:run(queue)
   end)

   wait(-1)
end