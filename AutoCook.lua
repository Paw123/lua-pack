script_name("AutoCook")
script_author("THERION")
script_description("���� ������� ��� � �������� �� � 24/7")
script_url("https://gitlab.com/THXRION666/lua-pack")

-- you can customize this
local cmd = string.lower(thisScript().name) -- activation command
local dialog_delay = 50     -- recommended to be above your average ping
local text3d_reach =  0.81  -- seems to be accurate you can try and customize it

dialog_delay = dialog_delay * 2
--- data below should be up to date for proper work
local GALAXY_IP = {"176.32.39.200", "176.32.39.199", "176.32.39.198"}

local COOK_3DTEXT_ID = 1024
-- dialog items start with 0
local PIZZA = 4
local STEAK = 3
local FRUIT_START = 5

local KITCHEN = {
   title   = "{FFFFFF}�����",
   button1 = "��������",
   button2 = "������",
   style   = 2,
}

local SHOP = {
   title   = "{FFFFFF}������� 24-7",
   button1 = "������",
   button2 = "�������",
   style   = 5,
}

local COOK_CMD = "/cook"
local COOK_STORE_STRING = "������������� ���� $100"
---

--- lib
local events = require("samp.events")
local inicfg = require("inicfg")
---
local ini = inicfg.load( {global = { on = true } }, thisScript().name .. ".ini")

---
local buy_state = false
local prepared_recently = false
local process_state = false
---

-- prints message to SAMP Chat
local function log(msg)
   local format = "{FB4343}[%s]{FFFFFF}: %s{FFFFFF}."
   sampAddChatMessage(string.format(format, thisScript().name, msg), -1)
end

local function is_in_array(array, value)
   for _, element in ipairs(array) do
      if value == element then
         return true
      end
   end
   return false
end


function main()
   do
      if not doesDirectoryExist("moonloader\\config") then 
         createDirectory("moonloader\\config") 
      end
      if not doesFileExist(thisScript().name .. ".ini") then 
         inicfg.save(ini, thisScript().name .. ".ini") 
      end
   end
   
   while not isSampAvailable() do wait(0) end

   do
      local ip, _ = sampGetCurrentServerAddress()
      if not is_in_array(GALAXY_IP, ip) then
         thisScript():unload()
      end
   end
   
   sampRegisterChatCommand(cmd,
   function()
      ini.global.on = not ini.global.on
      inicfg.save(ini, thisScript().name .. ".ini")
      log(ini.global.on and "ON" or "OFF")
   end)

   while true do wait(0)
      if ini.global.on and getCharActiveInterior(PLAYER_PED) ~= 0 then
         local search = sampIs3dTextDefined(COOK_3DTEXT_ID) and not prepared_recently
         if search then
            local x, y, z = getCharCoordinates(PLAYER_PED)
            local string, _, xi, yi, zi, _, _, _, _ = sampGet3dTextInfoById(COOK_3DTEXT_ID)
            local dist = (xi - x) ^ 2 + (yi - y) ^ 2 + (zi - z) ^ 2

            if dist < text3d_reach and string == COOK_CMD or string:find(COOK_STORE_STRING) then
               prepared_recently = true

               lua_thread.create(
               function()
                  wait(dialog_delay * 50)
                  prepared_recently = false
               end)

               sampSendChat(COOK_CMD)
            end
         end

         if process_state then
            for i = 1, 5 do
               sampSetCurrentDialogListItem(0)
               sampCloseCurrentDialogWithButton(1)
               wait(dialog_delay)
            end
            process_state = false
         end
      end
   end
end

events.onShowDialog = function(dialog_id, style, title, button1, button2, text)
   if not ini.global.on then 
      return {dialog_id, style, title, button1, button2, text} 
   end

   if style == SHOP.style and button1 == SHOP.button1 and button2 == SHOP.button2 and title == SHOP.title and not buy_state then
      lua_thread.create(
      function()
         buy_state = true
         -- fruits
         for i = FRUIT_START + 3, FRUIT_START, -1 do
            sampSetCurrentDialogListItem(i)
            sampCloseCurrentDialogWithButton(1)
            wait(dialog_delay)
         end
         -- fruits--
         -- pizza--
         for i = 1, 3 do
            sampSetCurrentDialogListItem(PIZZA)
            sampCloseCurrentDialogWithButton(1)
            wait(dialog_delay)
         end
         -- pizza
         -- steak
         sampSetCurrentDialogListItem(STEAK)
         sampCloseCurrentDialogWithButton(1)
         wait(dialog_delay)
         -- steak
         -- exit
         sampCloseCurrentDialogWithButton(0)
         wait(dialog_delay)
         --
         buy_state = false
      end)
   end

   if style == KITCHEN.style and button1 == KITCHEN.button1 and button2 == KITCHEN.button2 and title == KITCHEN.title then
      process_state = true 
   end
end