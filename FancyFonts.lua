script_name("FancyFonts")
script_author("THERION")
script_description("Forces gametext style to 4") -- looks fine on GY

local GALAXY_IP = {"176.32.39.200", "176.32.39.199", "176.32.39.198"}

-- check if value is in array
local function is_in_array(array, value)
   for _, element in ipairs(array) do
      if value == element then
         return true
      end
   end
   return false
end

local sampev = require("samp.events")

sampev.onDisplayGameText = function(style, time, text)
	return {4, time, text}
end

function main()
   repeat wait(0) until isSampAvailable()

   do -- check if GALAXY_RPG
      local ip, _ = sampGetCurrentServerAddress()
      if not is_in_array(GALAXY_IP, ip) then
         thisScript():unload()
      end
   end

	wait(-1)
end